package org.korzh.hw13;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class JSAlertsTests {
    protected WebDriver driver;
    @BeforeClass
    public void beforeClass(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @AfterClass
    public void afterClass(){
        if (driver != null){
            driver.quit();
        }
    }
    @BeforeMethod
    public void beforeMethod(){
        driver.get("https://the-internet.herokuapp.com/javascript_alerts");
        //driver.get("http://localhost:7080/javascript_alerts");
    }
    @Test (priority = 1)
    public void jsClickOnConfirmButtonOk(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsConfirm()";
        jsExecutor.executeScript(jscript);
        driver.switchTo().alert()
                .accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Ok");
    }
    @Test (priority = 2)
    public void jsClickOnConfirmButtonOk2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        driver.switchTo().alert()
                .accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Ok");
    }
    @Test (priority = 3)
    public void jsClickOnConfirmButtonCancel(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsConfirm()";
        jsExecutor.executeScript(jscript);
        driver.switchTo().alert()
                .dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Cancel");
    }
    @Test (priority = 4)
    public void jsClickOnConfirmButtonCancel2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        driver.switchTo().alert()
                .dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Cancel");
    }
    @Test (priority = 5)
    public void jsEnterTextIntoPrompt(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsPrompt()";
        jsExecutor.executeScript(jscript);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Hi. Remember me?");
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: Hi. Remember me?");
    }
    @Test (priority = 6)
    public void jsEnterTextIntoPrompt2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Hi. Remember me?");
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: Hi. Remember me?");
    }
    @Test (priority = 7)
    public void jsNoTextIntoPrompt(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsPrompt()";
        jsExecutor.executeScript(jscript);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered:");
    }
    @Test (priority = 8)
    public void jsNoTextIntoPrompt2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered:");
    }
    @Test (priority = 9)
    public void jsEnterTextIntoPromptCancel(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsPrompt()";
        jsExecutor.executeScript(jscript);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("some text");
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }
    @Test (priority = 10)
    public void jsEnterTextIntoPromptCancel2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("some text");
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }
    @Test (priority = 11)
    public void jsNoTextIntoPromptCancel(){
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String jscript = "jsPrompt()";
        jsExecutor.executeScript(jscript);
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }
    @Test (priority = 12)
    public void jsNoTextIntoPromptCancel2(){
        WebElement element = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].click();", element);
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }

}