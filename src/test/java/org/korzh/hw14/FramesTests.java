package org.korzh.hw14;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.Objects;

public class FramesTests {
    protected WebDriver driver;
    @BeforeClass
    public void beforeClass(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @AfterClass
    public void afterClass(){
        if (driver != null){
            driver.quit();
        }
    }
    @BeforeMethod
    public void beforeMethod(){
        driver.get("https://the-internet.herokuapp.com/nested_frames");
        //driver.get("http://localhost:7080/javascript_alerts");
    }
    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][]{
                {"frame-left","LEFT"},
                {"frame-middle","MIDDLE"},
                {"frame-right","RIGHT"},
                {"frame-bottom","BOTTOM"},
        };
    }
    @Test (dataProvider = "dataProvider")
    public void frameTestDP(String slaveFrame, String content){
        if (Objects.equals(slaveFrame, "frame-bottom")){
            driver.switchTo().frame(slaveFrame);
        }
        else {
            driver.switchTo().frame("frame-top")
                    .switchTo().frame(slaveFrame);
        }
        String txt = driver.findElement(By.xpath("//body")).getText();
        Assert.assertEquals(txt, content);
    }
}