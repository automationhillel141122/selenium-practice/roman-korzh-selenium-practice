package org.korzh.hw12;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
public class AlertsTests {
    protected WebDriver driver;
    @BeforeClass
    public void beforeClass(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @AfterClass
    public void afterClass(){
        if (driver != null){
            driver.quit();
        }
    }
    @BeforeMethod
    public void beforeMethod(){
        driver.get("https://the-internet.herokuapp.com/javascript_alerts");
        //driver.get("http://localhost:7080/javascript_alerts");
    }
    @Test (priority = 1)
    public void clickOnConfirmButtonOk(){
        driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();
        driver.switchTo().alert()
                .accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Ok");
    }
    @Test (priority = 2)
    public void clickOnConfirmButtonCancel(){
        driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();
        driver.switchTo().alert()
                .dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You clicked: Cancel");
    }
    @Test (priority = 3)
    public void enterTextIntoPrompt(){
        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Hi. Remember me?");
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: Hi. Remember me?");
    }
    @Test (priority = 4)
    public void noTextIntoPrompt(){
        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered:");
    }
    @Test (priority = 5)
    public void enterTextIntoPromptCancel(){
        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("some text");
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }
    @Test (priority = 6)
    public void noTextIntoPromptCancel(){
        driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
        String alertText = driver.findElement(By.id("result")).getText();
        Assert.assertEquals(alertText, "You entered: null");
    }

}